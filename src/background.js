import store from './store'
import * as types from './store/mutation-types'

global.browser = require('webextension-polyfill')

function onTabActive(activeInfo) {
  chrome.tabs.detectLanguage(activeInfo.tabId, detectLanguage)
}

function onTabUpdated(tabId, changeInfo, tab) {
  if (changeInfo.url !== undefined) {
    chrome.tabs.detectLanguage(tabId, detectLanguage)
  }
}

function detectLanguage(language) {
  store.commit(types.CHANGE_LANGUAGE, language)
  console.log(language)
}

chrome.tabs.onActivated.addListener(onTabActive)
chrome.tabs.onUpdated.addListener(onTabUpdated)

