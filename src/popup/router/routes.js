import Login from './pages/Login'
import Menu from './pages/Menu'

export default [
  {
    path: '/',
    component: Login
  },
  {
    path: '/loggedin',
    component: Menu
  }
]
