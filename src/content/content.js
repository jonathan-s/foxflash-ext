import Vue from 'vue';
import App from './App';
import store from '../store'

global.browser = require('webextension-polyfill');

const style = document.createElement('link');
style.rel = 'stylesheet';
style.type = 'text/css';
style.href = chrome.extension.getURL('content/content.css');
document.head.appendChild(style);

const vueApp = document.createElement("div");
vueApp.setAttribute("id", "foxflashvue");
document.body.appendChild(vueApp);

Vue.config.keyCodes.foxshift = 16;

/* eslint-disable no-new */
new Vue({
  el: '#foxflashvue',
  store,
  render: h => h(App),
});
