import Vue from 'vue';
import store from '../store'
import Dictionary from './FoxDictionary';
import axios from 'axios';

axios.defaults.baseURL = process.env.VUE_APP_URL;
global.browser = require('webextension-polyfill');

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  render: h => h(Dictionary),
});
