import * as types from './mutation-types'

export const setLoggedIn = ({commit}, payload) => {
  commit(types.CHANGE_LOGIN, payload);
}

export const setLookupWord = ({commit}, payload) => {
  commit(types.LOOKUP_WORD, payload)
}

export const setLanguage = ({commit}, payload) => {
  commit(types.CHANGE_LANGUAGE, payload)
}
