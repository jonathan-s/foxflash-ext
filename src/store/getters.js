export const loggedIn = (state) => state.loggedIn
export const lookupWord = (state) => state.lookupWord
export const language = (state) => state.language
