import * as types from './mutation-types'

export default {
  [types.CHANGE_LOGIN] (state, status) {
    state.loggedIn = status
  },
  [types.LOOKUP_WORD] (state, word) {
    state.lookupWord = word
  },
  [types.CHANGE_LANGUAGE] (state, lang) {
    state.language = lang
  }
}
